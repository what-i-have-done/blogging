## 待记录之物

* [ ] CS106A 第一周学习记录


## 待实施之物

* [ ]  实施文件服务器在 ESXi 上
- BT 下载： [aria2](https://aria2.github.io/)
    - [MANUAL](https://aria2.github.io/manual/en/html/index.html)
    - Web UIs: [webui-aria2](https://github.com/ziahamza/webui-aria2) OR [AriaNg](https://github.com/mayswind/AriaNg)
- 增量备份： [BorgBackup](https://borgbackup.readthedocs.io/en/stable/)
  - [实例参考：BorgBackup —— 增量备份方案](https://wzyboy.im/post/1106.html)
- 文件同步： [Syncthing](https://syncthing.net/)
  - [MANUAL](https://docs.syncthing.net/)
- 文件共享：
  - [NFS](http://nfs.sourceforge.net/)
  - [Samba](https://www.samba.org/)
- 音乐服务：[Music Player Daemon](https://github.com/MusicPlayerDaemon/MPD)
- 监控系统：
  - [实例参考：collectd + Graphite + Grafana 搭建网络质量监控系统](https://wzyboy.im/post/1084.html)
- DNS & Proxy: 
  - [Dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html)
  - ShadowsocksR

## 待学习之物

* [ ] ESXi 基本使用
* [ ] Git 基本使用
